# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :infosec_lab,
  ecto_repos: [InfosecLab.Repo]

# Configures the endpoint
config :infosec_lab, InfosecLabWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "h6lV0hm5Y3Tb/DW0k+fO/iMV/J0zfrxHY8CmHC8mpaFJHMPh9JTGTqS6DFqrrNiq",
  render_errors: [view: InfosecLabWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: InfosecLab.PubSub,
  live_view: [signing_salt: "/4XzTrxr"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
