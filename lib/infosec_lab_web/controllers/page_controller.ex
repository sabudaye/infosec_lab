defmodule InfosecLabWeb.PageController do
  use InfosecLabWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
