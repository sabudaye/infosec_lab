defmodule InfosecLab.Repo do
  use Ecto.Repo,
    otp_app: :infosec_lab,
    adapter: Ecto.Adapters.Postgres
end
